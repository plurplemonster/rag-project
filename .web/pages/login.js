/** @jsxImportSource @emotion/react */


import { Fragment, useCallback, useContext } from "react"
import { Fragment_fd0e7cb8f9fb4669a6805377d925fba0 } from "/utils/stateful_components"
import { Box, Button, Input, Link, Text, VStack } from "@chakra-ui/react"
import "focus-visible/dist/focus-visible"
import { EventLoopContext } from "/utils/context"
import { Event, set_val } from "/utils/state"
import NextLink from "next/link"
import NextHead from "next/head"



export function Button_63f2cbe306a9e502688ffc5bcafffab9 () {
  const [addEvents, connectError] = useContext(EventLoopContext);

  const on_click_255658953d8a043f82f5b4364a8aa6e1 = useCallback((_e) => addEvents([Event("state.state.login", {})], (_e), {}), [addEvents, Event])

  return (
    <Button onClick={on_click_255658953d8a043f82f5b4364a8aa6e1} sx={{"bg": "blue.500", "color": "white", "_hover": {"bg": "blue.600"}}}>
  {`Log in`}
</Button>
  )
}

export function Input_2b420a1e92500217ea87edabcdaadd0f () {
  const [addEvents, connectError] = useContext(EventLoopContext);

  const on_blur_98ddeaa866dcbe88d6fa21cfe04ca51f = useCallback((_e0) => addEvents([Event("state.state.set_password", {value:_e0.target.value})], (_e0), {}), [addEvents, Event])

  return (
    <Input onBlur={on_blur_98ddeaa866dcbe88d6fa21cfe04ca51f} placeholder={`Password`} sx={{"mb": 4}} type={`password`}/>
  )
}

export function Input_d4e788e3f5184c94b0a7d0497e14c294 () {
  const [addEvents, connectError] = useContext(EventLoopContext);

  const on_blur_bb8ca1ee4fa7746d3b50488324f1c616 = useCallback((_e0) => addEvents([Event("state.state.set_username", {value:_e0.target.value})], (_e0), {}), [addEvents, Event])

  return (
    <Input onBlur={on_blur_bb8ca1ee4fa7746d3b50488324f1c616} placeholder={`Username`} sx={{"mb": 4}} type={`text`}/>
  )
}

export default function Component() {

  return (
    <Fragment>
  <Fragment_fd0e7cb8f9fb4669a6805377d925fba0/>
  <VStack>
  <VStack justify={`center`} sx={{"width": "100%", "height": "100vh", "align": "center"}}>
  <Text as={`span`}>
  {`Welcome!`}
</Text>
  <Text as={`span`}>
  {`Sign in or sign up to get started.`}
</Text>
  <Box/>
  <Box sx={{"alignItems": "center", "bg": "white", "border": "1px solid #eaeaea", "p": 4, "maxWidth": "400px", "borderRadius": "lg"}}>
  <Input_d4e788e3f5184c94b0a7d0497e14c294/>
  <Input_2b420a1e92500217ea87edabcdaadd0f/>
  <Button_63f2cbe306a9e502688ffc5bcafffab9/>
</Box>
  <Text sx={{"color": "gray.600"}}>
  {`Don't have an account yet?`}
  <Link as={NextLink} href={`/signup`} sx={{"color": "blue.500"}}>
  {`Sign up here.`}
</Link>
</Text>
</VStack>
</VStack>
  <NextHead>
  <title>
  {`Reflex App`}
</title>
  <meta content={`A Reflex app.`} name={`description`}/>
  <meta content={`favicon.ico`} property={`og:image`}/>
</NextHead>
</Fragment>
  )
}
