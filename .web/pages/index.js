/** @jsxImportSource @emotion/react */


import { Fragment, useCallback, useContext, useRef } from "react"
import { Fragment_fd0e7cb8f9fb4669a6805377d925fba0 } from "/utils/stateful_components"
import { Alert, AlertIcon, AlertTitle, Avatar, Box, Button, Center, Drawer, DrawerBody, DrawerContent, DrawerFooter, DrawerHeader, DrawerOverlay, Heading, HStack, Image as ChakraImage, Input, Link, Menu, MenuButton, MenuDivider, MenuItem, MenuList, Spacer, Spinner, Text, VStack } from "@chakra-ui/react"
import "@radix-ui/themes/styles.css"
import "focus-visible/dist/focus-visible"
import { BellIcon, HamburgerIcon, QuestionIcon, Search2Icon, SearchIcon } from "@chakra-ui/icons"
import { EventLoopContext, StateContexts } from "/utils/context"
import { Event, getRefValue, getRefValues, isTrue, refs, set_val } from "/utils/state"
import NextLink from "next/link"
import { Theme as RadixThemesTheme } from "@radix-ui/themes"
import theme from "/utils/theme.js"
import NextHead from "next/head"



export function Box_b10a75ef368105be79a41e39479b5107 () {
  const [addEvents, connectError] = useContext(EventLoopContext);
  
    const handleSubmit_cc5171f9748eae91892c1784771b5fda = useCallback((ev) => {
        const $form = ev.target
        ev.preventDefault()
        const form_data = {...Object.fromEntries(new FormData($form).entries()), ...{"search_input": getRefValue(refs['ref_search_input'])}}

        addEvents([Event("state.state.handle_submit", {form_data:form_data})])

        if (false) {
            $form.reset()
        }
    })
    
  const ref_search_input = useRef(null); refs['ref_search_input'] = ref_search_input;


  return (
    <Box as={`form`} onSubmit={handleSubmit_cc5171f9748eae91892c1784771b5fda} sx={{"flex": 12}}>
  <Center>
  <Input id={`search_input`} placeholder={`Search here...`} ref={ref_search_input} sx={{"flex": 4}} type={`text`}/>
  <Button sx={{"width": "45%", "flex": 0.5}} type={`submit`}>
  <Search2Icon/>
</Button>
</Center>
</Box>
  )
}

export function Button_482e640509f1f833d05fe8f08b7108a9 () {
  const [addEvents, connectError] = useContext(EventLoopContext);

  const on_click_2905983f8758758258aab6a80fcc9a4c = useCallback((_e) => addEvents([Event("state.state.toggle_drawer", {})], (_e), {}), [addEvents, Event])

  return (
    <Button onClick={on_click_2905983f8758758258aab6a80fcc9a4c}>
  {`Close`}
</Button>
  )
}

export function Fragment_0766fc3a2b5213d634fbcd7dc98a511f () {
  const state__state = useContext(StateContexts.state__state)


  return (
    <Fragment>
  {isTrue(state__state.valid_input) ? (
  <Fragment>
  <HStack alignItems={`start`} justify={`end`} sx={{"justifyContents": "center"}}>
  <Box sx={{"width": "23%"}}/>
  <VStack>
  <Fragment>
  {isTrue(state__state.has_summary) ? (
  <Fragment>
  <Box sx={{"boxShadow": "0px 0px 4px rgba(0, 0, 0, 0.1)", "bg": "white", "borderRadius": "15px", "borderColor": "lightgrey", "borderWidth": "thin", "padding": 5, "width": "620px"}}>
  <Heading size={`sm`} sx={{"color": "blue"}}>
  {state__state.cafe_name}
</Heading>
  <Text>
  {state__state.summary}
</Text>
</Box>
</Fragment>
) : (
  <Fragment>
  <Box/>
</Fragment>
)}
</Fragment>
  <Fragment>
  {isTrue(state__state.is_result_empty) ? (
  <Fragment>
  <Box sx={{"bg": "white", "borderRadius": "15px", "borderColor": "whitesmoke", "borderWidth": "thin", "padding": 5, "width": "620px"}}>
  <Text>
  {`No results found`}
</Text>
</Box>
</Fragment>
) : (
  <Fragment>
  {isTrue((state__state.intent_type === "default_embedding_search")) ? (
  <Fragment>
  <VStack>
  {state__state.search_result_list.map((item, index_3f4df664d0ae377809b6199592a7f2b3) => (
  <Box key={index_3f4df664d0ae377809b6199592a7f2b3} sx={{"bg": "white", "borderRadius": "15px", "borderColor": "whitesmoke", "borderWidth": "thin", "padding": 5, "width": "620px"}}>
  <Link as={NextLink} href={`www.google.com`} sx={{"fontSize": "1.1rem", "color": "rgb(107,99,246)"}}>
  {item["cafeName"]}
</Link>
  <Text>
  {("Rating: " + item["cafeRating"])}
</Text>
  <Fragment>
  {isTrue(item["city"]) ? (
  <Fragment>
  <Text>
  {("City: " + item["city"])}
</Text>
</Fragment>
) : (
  <Fragment/>
)}
</Fragment>
  <Fragment>
  {isTrue(item["rateForTwo"]) ? (
  <Fragment>
  <Text>
  {("Rate for two: " + item["rateForTwo"])}
</Text>
</Fragment>
) : (
  <Fragment/>
)}
</Fragment>
  <Fragment>
  {isTrue(item["cuisines"]) ? (
  <Fragment>
  <Text>
  {("Cuisines  " + item["cuisines"])}
</Text>
</Fragment>
) : (
  <Fragment/>
)}
</Fragment>
  <Text>
  {("Reviews: " + item["reviews"])}
</Text>
</Box>
))}
</VStack>
</Fragment>
) : (
  <Fragment>
  {isTrue((state__state.intent_type === "find_cafe_reviews") || (state__state.intent_type === "find_top_rated_cafe_by_cuisine")) ? (
  <Fragment>
  <VStack>
  {state__state.reviews_list.map((item, index_6644d727def43386f9830afe0119130e) => (
  <Box key={index_6644d727def43386f9830afe0119130e} sx={{"bg": "white", "borderRadius": "15px", "borderColor": "whitesmoke", "borderWidth": "thin", "padding": 5, "width": "620px"}}>
  <Link as={NextLink} href={`www.google.com`} sx={{"fontSize": "1.1rem", "color": "rgb(107,99,246)"}}>
  {`Review`}
</Link>
  <Text>
  {JSON.stringify(item)}
</Text>
</Box>
))}
</VStack>
</Fragment>
) : (
  <Fragment>
  <VStack>
  {state__state.search_result_list.map((item, index_c623644ad40dcf8556c5c70587304fb9) => (
  <Box key={index_c623644ad40dcf8556c5c70587304fb9} sx={{"bg": "white", "borderRadius": "15px", "borderColor": "whitesmoke", "borderWidth": "thin", "padding": 5, "width": "620px"}}>
  <Link as={NextLink} href={`www.google.com`} sx={{"fontSize": "1.1rem", "color": "rgb(107,99,246)"}}>
  {item["cafeName"]}
</Link>
  <Text>
  {("Rating: " + item["cafeRating"])}
</Text>
  <Text>
  {("City: " + item["city"])}
</Text>
  <Text>
  {("Rate for two: " + item["rateForTwo"])}
</Text>
  <Text>
  {("Cuisines: " + item["cuisines"])}
</Text>
</Box>
))}
</VStack>
</Fragment>
)}
</Fragment>
)}
</Fragment>
)}
</Fragment>
</VStack>
  <Fragment>
  {isTrue(state__state.has_kp) ? (
  <Fragment>
  <Box sx={{"boxShadow": "0px 4px 6px rgba(0, 0, 0, 0.1)", "bg": "white", "borderRadius": "15px", "borderColor": "lightgrey", "borderWidth": "thin", "padding": 5, "width": "500px", "display": ["none", "none", "none", "flex", "flex", "flex"]}}>
  <VStack alignItems={`left`} sx={{"align": "start"}}>
  <Heading size={`md`}>
  {state__state.cafe_name}
</Heading>
  <ChakraImage src={`https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRtdnR2i_9aipQLrEkLzm7NNxzy6mwhNjDYijEQMLgPMEwr6MsY7SiqXSuEDyTEjO9GKfU&usqp=CAU`} sx={{"width": "auto", "height": "auto"}}/>
  <Text>
  {("City: " + state__state.cafe_city)}
</Text>
  <Text>
  {("Rating: " + state__state.cafe_rating)}
</Text>
  <Text>
  {("Rate for two: " + state__state.cafe_rateForTwo)}
</Text>
</VStack>
</Box>
</Fragment>
) : (
  <Fragment/>
)}
</Fragment>
  <Box sx={{"width": "10%"}}/>
</HStack>
</Fragment>
) : (
  <Fragment>
  <Box>
  <Spinner size={`xl`} speed={`0.5s`} sx={{"color": "lightgreen"}} thickness={5}/>
</Box>
</Fragment>
)}
</Fragment>
  )
}

export function Fragment_71584185f15334065fa7d6667fd2ed44 () {
  const state__state = useContext(StateContexts.state__state)


  return (
    <Fragment>
  {isTrue((state__state.alert_message !== "")) ? (
  <Fragment>
  <Alert status={`error`}>
  <AlertIcon/>
  <AlertTitle>
  {state__state.alert_message}
</AlertTitle>
</Alert>
</Fragment>
) : (
  <Fragment/>
)}
</Fragment>
  )
}

export function Drawer_8311bb5bd0e23f0e733fa84cd5e922e2 () {
  const state__state = useContext(StateContexts.state__state)
  const [addEvents, connectError] = useContext(EventLoopContext);

  const on_overlay_click_2905983f8758758258aab6a80fcc9a4c = useCallback((_e) => addEvents([Event("state.state.toggle_drawer", {})], (_e), {}), [addEvents, Event])
  const on_esc_2905983f8758758258aab6a80fcc9a4c = useCallback((_e) => addEvents([Event("state.state.toggle_drawer", {})], (_e), {}), [addEvents, Event])

  return (
    <Drawer isOpen={state__state.drawer_open} onEsc={on_esc_2905983f8758758258aab6a80fcc9a4c} onOverlayClick={on_overlay_click_2905983f8758758258aab6a80fcc9a4c} placement={`left`}>
  <DrawerOverlay/>
  <DrawerContent sx={{"bg": "rgba(255, 255, 255, 1.0)"}}>
  <DrawerHeader>
  {`Logo`}
</DrawerHeader>
  <DrawerBody>
  <Menu>
  <Link as={``}>
  <MenuItem>
  <SearchIcon sx={{"mr": 3}}/>
  {`Browse`}
</MenuItem>
</Link>
  <Link as={NextLink} href={`/login`}>
  <MenuItem>
  <QuestionIcon sx={{"mr": 3}}/>
  {`Login`}
</MenuItem>
</Link>
</Menu>
</DrawerBody>
  <DrawerFooter>
  <Button_482e640509f1f833d05fe8f08b7108a9/>
</DrawerFooter>
</DrawerContent>
</Drawer>
  )
}

export function Hamburgericon_999d2b36fde2f4aa44d8fa9592271782 () {
  const [addEvents, connectError] = useContext(EventLoopContext);

  const on_click_2905983f8758758258aab6a80fcc9a4c = useCallback((_e) => addEvents([Event("state.state.toggle_drawer", {})], (_e), {}), [addEvents, Event])

  return (
    <HamburgerIcon onClick={on_click_2905983f8758758258aab6a80fcc9a4c} sx={{"width": "1.5em", "height": "1.5em", "_hover": {"cursor": "pointer"}, "display": "flex", "mr": 3}}/>
  )
}

export default function Component() {

  return (
    <Fragment>
  <Fragment_fd0e7cb8f9fb4669a6805377d925fba0/>
  <VStack justify={`start`}>
  <Box sx={{"bg": "rgba(255,255,255, 0.9)", "backdropFilter": "blue(10px)", "paddingY": ["0.8em", "0.8em", "0.5em"], "borderBottom": "1px solid #F4F3F6", "width": "100%", "position": "sticky", "zIndex": "999", "top": "0"}}>
  <HStack alignItems={`center`}>
  <Spacer sx={{"flex": 1}}/>
  <Hamburgericon_999d2b36fde2f4aa44d8fa9592271782/>
  <Spacer sx={{"flex": 4, "display": ["none", "none", "none", "flex", "flex"]}}/>
  <Spacer sx={{"flex": 0.5}}/>
  <Box_b10a75ef368105be79a41e39479b5107/>
  <Spacer sx={{"flex": 3}}/>
  <HStack alignItems={`center`} spacing={`2em`} sx={{"display": ["none", "none", "none", "flex", "flex", "flex", "flex"]}}>
  <Button variant={`ghost`}>
  <BellIcon/>
</Button>
  <Menu>
  <MenuButton>
  <Avatar name={`John Doe`} size={`sm`}/>
</MenuButton>
  <MenuList>
  <MenuItem>
  {`My profile`}
</MenuItem>
  <MenuDivider/>
  <MenuItem>
  {`Settings`}
</MenuItem>
  <MenuItem>
  {`Help`}
</MenuItem>
</MenuList>
</Menu>
</HStack>
  <Spacer sx={{"flex": 1}}/>
</HStack>
</Box>
  <Drawer_8311bb5bd0e23f0e733fa84cd5e922e2/>
  <Fragment_71584185f15334065fa7d6667fd2ed44/>
  <Fragment_0766fc3a2b5213d634fbcd7dc98a511f/>
</VStack>
  <NextHead>
  <title>
  {`Reflex App`}
</title>
  <meta content={`A Reflex app.`} name={`description`}/>
  <meta content={`favicon.ico`} property={`og:image`}/>
</NextHead>
</Fragment>
  )
}
