import reflex as rx

from rag_system.state import State
from typing import Dict

def box_style() -> dict:
    return{
        "bg": "white",
        "border_radius": "15px",
        "border_color": "whitesmoke",
        "border_width": "thin",
        "padding": 5,
        "width": "620px"
    }
    
# def searchResult() -> rx.Component:
#     return rx.cond(
#         State.is_result_empty,
#         noResultFoundTemplate(),
#         finalizedSearchResultTemplate(),
#     )

def searchResult() -> rx.Component:
    return rx.cond(
        State.is_result_empty,
        noResultFoundTemplate(),
        finalizedSearchResultTemplate(),
    )
    
def noResultFoundTemplate() -> rx.Component:
    styles = box_style()
    return rx.box(
        rx.text("No results found"),
        **styles,
    )
    

def finalizedSearchResultTemplate() -> rx.Component:
    return rx.cond(
        (State.intent_type == "default_embedding_search"),
        rx.vstack(
            rx.foreach(State.search_result_list, SearchResultWithReviewTemplate)
        ),
        rx.cond(
            ((State.intent_type == "find_cafe_reviews") | (State.intent_type == "find_top_rated_cafe_by_cuisine")),
            rx.vstack( # This will print the specified text
                rx.foreach(State.reviews_list, reviewTemplate)
            ),
            rx.vstack(
                rx.foreach(State.search_result_list, searchResultTemplate)
            )
        )
    )
    
    
    
def reviewTemplate(item) -> rx.Component:
    styles = box_style()
    return rx.box(
        # rx.heading("Review", size="sm", color="blue"),
        rx.link(
                "Review",
                href="www.google.com",
                color="rgb(107,99,246)",
                style={"font_size": "1.1rem"},
        ),
        rx.text(item.to_string()),
        # rx.link(
        #     "www.chicken.com",
        #     href="www.chicken.com",
        #     color="rgb(34,139,34)",
        #     is_external=True,
        # ),
        **styles,
    )
    

    
# MOST RECENT 
# def finalizedSearchResultTemplate() -> rx.Component:
#     return rx.cond(
#         (State.intent_type == "default_embedding_search") | (State.intent_type == "find_cafe_reviews"),
#         rx.vstack(
#             rx.foreach(State.search_result_list, SearchResultWithReviewTemplate)
#         ),
#         rx.vstack(
#             rx.foreach(State.search_result_list, searchResultTemplate)
#         )
#     )



    
def SearchResultWithReviewTemplate(item: Dict[str, str]) -> rx.Component:
        styles = box_style()
        return rx.box(
            # rx.heading(item["cafeName"], size="sm", color="blue"),
            rx.link(
                item["cafeName"],
                href="www.google.com",
                color="rgb(107,99,246)",
                style={"font_size": "1.1rem"},
            ),
            
            rx.text("Rating: " + item["cafeRating"]),
            
            rx.cond(
                item["city"],
                rx.text("City: " + item["city"]),
                None,
            ),
            
            rx.cond(
                item["rateForTwo"],
                rx.text("Rate for two: " + item["rateForTwo"]),
                None,
            ),
            
            rx.cond(
                item["cuisines"],
                rx.text("Cuisines  " + item["cuisines"]),
                None,
            ),
            
            rx.text("Reviews: " + item["reviews"]),
            # rx.link(
            #     "www.google.com",
            #     href="www.google.com",
            #     color="rgb(34,139,34)",
            #     is_external=True,
            # ),
            **styles,
            # rx.text("City: " + item["city"]),
            # rx.text("Rate for two: " + item["rateForTwo"]),
            # # rx.text("Cuisines: " + item["cuisines"]),
        )
    
def searchResultTemplate(item: Dict[str, str]) -> rx.Component:
    styles = box_style()
    return rx.box(
        # rx.heading(item["cafeName"], size="sm", color="blue"),
        rx.link(
            item["cafeName"],
            href="www.google.com",
            color="rgb(107,99,246)",
            style={"font_size": "1.1rem"},
        ),
        rx.text("Rating: " + item["cafeRating"]),
        
        rx.text("City: " + item["city"]),
        rx.text("Rate for two: " + item["rateForTwo"]),
        rx.text("Cuisines: " + item["cuisines"]),
        # rx.link(
        #     "www.google.com",
        #     href="www.google.com",
        #     color="rgb(34,139,34)",
        #     is_external=True,
        # ),
        **styles,
    )
    
    
    