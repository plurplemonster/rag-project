import reflex as rx
import time
import asyncio

from rag_system.state import State

def box_style() -> dict:
    return{
        # "bg": "rgb(250, 250, 250)",
        "bg": "white",
        "border_radius": "15px",
        "border_color": "lightgrey",
        "border_width": "thin",
        "padding": 5,
        "style": {
            "boxShadow": "0px 0px 4px rgba(0, 0, 0, 0.1)",
        },
        "width": "620px",
    }
    
def definition_template() -> rx.Component:
    styles = box_style()
    return rx.box(
        rx.heading(State.cafe_name, size="sm", color="blue"),
        rx.text(State.summary),
        
        # rx.link(
        #     "www.google.com",
        #     color="rgb(34,139,34)",
        #     is_external=True,
        # ),
        **styles,
    )
    
def featuredSnippet() -> rx.Component:
    return rx.cond(
        State.has_summary,
        definition_template(),
        rx.box(),
    )    
