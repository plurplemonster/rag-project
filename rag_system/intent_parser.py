import json
import sys
import re

from adapt.intent import IntentBuilder
from adapt.engine import IntentDeterminationEngine

def get_search_intent(user_input):
    engine = IntentDeterminationEngine()
    
    search_keyword = [
        "find",
        "show",
        "get",
    ]
    
    for sk in search_keyword:
        engine.register_entity(sk, "SearchKeyword")
        
    cafe_keyword = [
        "cafe",
        "cafes",
        "restaurant",
        "restaurants",
        "shop",
        "eatery",
    ]
    
    for ck in cafe_keyword:
        engine.register_entity(ck, "CafeKeyword")
        
    cuisine_type_keyword = [
        # "Cafe",
        "Coffee",
        "Shake",
        "Juices",
        "Beverages",
        "Waffle",
        "Desserts",
        "Ice Cream",
        "North Indian",
        "Pizza",
        "Asian",
        "Chinese",
        "Italian",
        "Bakery",
        "Fast Food",
        "Continental",
        "Pasta",
        "Burger",
        "Sandwich",
        "Wraps",
        "Oriental",
        "Salad",
        "Mexican",
        "Tea",
        "Thai",
        "Biryani",
        "South Indian",
        "Street Food",
        "Panini",
        "Momos",
        "Healthy Food",
        "European",
        "Japanese",
        "American",
        "Sichuan",
        "Maharashtrian",
        "Bubble Tea",
        "Mediterranean",
        "Sushi",
        "Korean",
        "Rolls",
        "Pancake",
        "Lebanese",
        "BBQ",
        "Finger Food",
        "Middle Eastern",
        "Steak",
        "Bar Food",
        "French",
        "Belgian",
    ]
    
    for ctk in cuisine_type_keyword:
        engine.register_entity(ctk, "CuisineTypeKeyword")
        
    cuisine_keyword = [
        "food",
        "cuisine",
        "dish"
    ]
    
    for csk in cuisine_keyword:
        engine.register_entity(csk, "CuisineKeyword")
        
        
    find_cafe_by_cuisine_intent = IntentBuilder("find_cafe_by_cuisine_intent") \
        .require("SearchKeyword") \
        .require("CafeKeyword") \
        .require("CuisineTypeKeyword") \
        .optionally("CuisineKeyword") \
        .build()
        
    
    # ===============================================================
    
    top_rated_keyword = [
        "top",
        "best",
        "highest",
    ]
    
    for trk in top_rated_keyword:
        engine.register_entity(trk, "TopRatedKeyword")
        
    rating_keyword = [
        "rated",
        "review",
        "reviews",
        "ratings",
    ]
    
    for rk in rating_keyword:
        engine.register_entity(rk, "RatingKeyword")
        
        
    find_top_rated_cafe = IntentBuilder("find_top_rated_cafe") \
        .optionally("SearchKeyword") \
        .require("TopRatedKeyword") \
        .optionally("RatingKeyword") \
        .require("CafeKeyword") \
        .build()
        

    find_top_rated_cafe_by_cuisine = IntentBuilder("find_top_rated_cafe_by_cuisine") \
        .optionally("SearchKeyword") \
        .require("TopRatedKeyword") \
        .optionally("RatingKeyword") \
        .require("CuisineTypeKeyword") \
        .optionally("CuisineKeyword") \
        .optionally("CafeKeyword") \
        .build()
    
    # ------------------------------------------------------------------------------------------------
    
    find_cafe_reviews = IntentBuilder("find_cafe_reviews") \
        .require("SearchKeyword") \
        .require("RatingKeyword") \
        .build()
        # String format: Find Review for cafe XYZ
    
    engine.register_intent_parser(find_cafe_reviews)
    engine.register_intent_parser(find_cafe_by_cuisine_intent)
    engine.register_intent_parser(find_top_rated_cafe)
    engine.register_intent_parser(find_top_rated_cafe_by_cuisine)
    
    intent_results = list(engine.determine_intent(user_input))
    for intent in intent_results:
        if intent and intent.get('confidence') > 0:
            print(json.dumps(intent,indent=4))
            
    return intent_results