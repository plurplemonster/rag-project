import reflex as rx
import time
import asyncio
import httpx
import json
import re
from typing import List
from typing import Dict

from .intent_parser import get_search_intent

class State(rx.State):
    """The app state."""
    
    drawer_open: bool = False # Whether the drawer is open.
    
    username: str
    password: str
    search_input: str = ""
    valid_input: bool = True
    
    is_result_empty: bool = True
    search_result_list: List[Dict[str, str]] = []
    
    intent_results = []
    intent_type: str = ""
    cuisine_type: str = ""
    
    api_data: str = ""
    
    has_summary: bool = False
    summary: str = ""
    
    reviews_list = []
    
    state_response = []
    cafe_name: str = ""
    cafe_rating: str = ""
    cafe_city: str = ""
    cafe_rateForTwo = ""
    
    has_kp: bool = False
    
    alert_message: str = ""
    
    # is_review_empty: bool = True
    
    def toggle_drawer(self):
        self.drawer_open = not self.drawer_open
        
    # async def load(self):
    #     self.valid_input = False
    #     yield
    #     await asyncio.sleep(1)
    #     self.valid_input = True

    def login(self):
       """Log in a user."""
       with rx.session() as session:
           if self.password == "123":
               return rx.redirect("/")
           else:
               return rx.window_alert("Invalid username or password.")

    def validate_input(self):
        return bool(self.search_input)
    
    async def handle_submit(self, form_data: dict):
        self.intent_type = ""
        self.search_result_list = []
        self.intent_results = []
        self.cuisine_type = ""
        self.summary = ""
        self.cafe_name = ""
        self.cafe_rating = ""
        self.cafe_city = ""
        self.cafe_rateForTwo = ""
        self.alert_message = ""
        self.has_kp = False
        
        self.search_input = form_data["search_input"]
        # copy
        # self.is_review_empty = True
        self.valid_input = False
        yield
        await self.handle_search()
    
    async def handle_search(self):
        if self.validate_input():
            self.intent_results = get_search_intent(self.search_input)
            
            for intent in self.intent_results:
                if intent and intent.get('confidence') > 0:
                    self.intent_type = intent.get('intent_type')
                    if(self.intent_type=="find_cafe_by_cuisine_intent" or self.intent_type=="find_top_rated_cafe_by_cuisine"):
                        self.cuisine_type = intent.get("CuisineTypeKeyword")
            
            await self.get_data()
        else:
            self.valid_input = True
            self.is_result_empty = True
            self.has_summary = False
            
    
    async def get_data(self):
        response = await self.fetch_data()
        
        if response is None:
            print("NOTE: No response received.")
            self.valid_input = True
            return
        
        self.valid_input = True
        
        if self.intent_type == "find_top_rated_cafe_by_cuisine":
            self.state_response = response[0]
            
            self.summary = self.state_response.get("summary")
            self.cafe_name = self.state_response["cafeName"]
            self.cafe_rating = self.state_response["cafeRating"]
            self.cafe_city = self.state_response["city"]
            self.cafe_rateForTwo = self.state_response["rateForTwo"]
            self.reviews_list = self.state_response.get("reviews", [])
            self.has_summary = True
            self.is_result_empty = False
            self.has_kp = True
            
        elif self.intent_type == "find_cafe_reviews":
            self.state_response = response['data'][0]
            print("intent \n")
            self.cafe_name = response['data'][0]['cafeName']
            self.cafe_city = response['data'][0]['city']
            self.cafe_rating = response['data'][0]['cafeRating']
            self.cafe_rateForTwo = response['data'][0]['rateForTwo']
            self.has_kp = True
            
            self.has_summary = True
            self.is_result_empty = False
            
            self.reviews_list = response['data'][0]['reviews']
            self.summary = response['openai_summary']
            self.cafe_name = response['data'][0]['cafeName']
            
            print("==============================")
            print(self.cafe_name)
            print("RESPONSE NOW NOWN NOWNOWNOW")
            print(json.dumps(response,indent=2))
            
            
            
        elif self.intent_type == "default_embedding_search":
            
            self.summary = response['openai_summary']
            self.search_result_list = response['data']
            print(json.dumps(self.search_result_list, indent = 2))
            self.has_summary = True
            self.is_result_empty = False
            # self.is_review_empty = False
        
        else:
            self.has_summary = False
            self.search_result_list = response
            self.is_result_empty = False
            print(f"search result list type : {type(self.search_result_list)}")
            
        # self.handle_response(api_data)
        
    # def handle_response(self, search_results):
    #     if not search_results:
    #         is_result_empty = True
    #     else:
    #         self.search_result_list = search_results.get('searchResult', [])
    #         self.is_result_empty = len(self.search_result_list) == 0
      
      
        
    # async def fetch_data(self, search_query):
    #     async with httpx.AsyncClient() as client:
    #         response = await client.get(f"http://127.0.0.1:8001/search?keyword={search_query}")
    #         # http://127.0.0.1:8001/search?keyword=science
    #         print("search=" + search_query)
    #         print("response=" + str(response.json()))
    #         return response.json()
    
    # async def fetch_data(self):
        
    #     if self.intent_type == "find_cafe_by_cuisine_intent":
    #         url = f"http://127.0.0.1:8001/find_cafe_by_cuisine?cuisine_type={self.cuisine_type}"
            
    #     elif self.intent_type == "find_top_rated_cafe":
    #         url = f"http://127.0.0.1:8001/find_top_rated_cafe"
        
    #     elif self.intent_type == "find_top_rated_cafe_by_cuisine":
    #         url = f"http://127.0.0.1:8001/find_top_rated_cafe_by_cuisine?input={self.search_input}&cuisine_type={self.cuisine_type}"
        
    #     elif self.intent_type == "find_cafe_reviews":
    #         cafeName = self.search_input.split("for")[-1].strip()
    #         url = f"http://127.0.0.1:8001/find_cafe_review?cafeName={cafeName}"
        
    #     else:
    #         self.intent_type = "default_embedding_search"
    #         url = f"http://127.0.0.1:8001/search?keyword={self.search_input}"
            
    #     async with httpx.AsyncClient() as client:
    #         response = await client.get(url)
            
    #         return response.json()


    async def fetch_data(self):
        url = self.get_url()
        print(f"URL ==== {url}")
        
        try:
            start_time = time.time()
            # async with httpx.AsyncClient(timeout=10.0) as client:
            async with httpx.AsyncClient(timeout=10.0) as client:
                response = await client.get(url)
                end_time = time.time()
                
                # print(f"Time taken: {end_time - start_time} seconds")
                # return response.json()
                
                 # Check if the response is successful
            if response is not None and response.status_code == 200:
                try:
                    # Try to parse the response as JSON
                    return response.json()
                except httpx.JSONDecodeError as json_error:
                    self.alert_message = f"Failed to decode JSON response: {json_error}"
                    self.has_summary = False
                    self.is_result_empty = True
                    print(self.alert_message)
                    return None
            else:
                # Set alert message for no response
                self.alert_message = "No valid response received."
                print(self.alert_message)
                self.has_summary = False
                self.is_result_empty = True
                return None
            
        except httpx.ReadTimeout:
            # print("The request timed out. Please try again later.")
            self.alert_message = "The request timed out. Please try again later."
            print(self.alert_message)
            
            end_time = time.time()
            print(f"TIMEOUT: Time taken: {end_time - start_time} seconds")
            return None
        
        except httpx.HTTPStatusError as e:
            self.alert_message = f"HTTP error: {e}"
            # print(f"HTTP error: {e}")
            print(self.alert_message)
            return None
        
        except Exception as e:
            self.alert_message = f"An unexpected error occurred: {e}"
            # print(f"An unexpected error occurred: {e}")
            print(self.alert_message)
            return None
        
    
    def get_url(self):
        if self.intent_type == "find_cafe_by_cuisine_intent":
            url = f"http://127.0.0.1:8001/find_cafe_by_cuisine?cuisine_type={self.cuisine_type}"
                
        elif self.intent_type == "find_top_rated_cafe":
            url = f"http://127.0.0.1:8001/find_top_rated_cafe"
            
        elif self.intent_type == "find_top_rated_cafe_by_cuisine":
            url = f"http://127.0.0.1:8001/find_top_rated_cafe_by_cuisine?input={self.search_input}&cuisine_type={self.cuisine_type}"
            
        elif self.intent_type == "find_cafe_reviews":
            match = re.search(r'\"([^\"]+)\"', self.search_input)
            # cafeName = self.search_input.split()[-1]
            if match is not None:
                cafeName = match.group(1)
                print(f"Cafe Name: {cafeName}")
                url = f"http://127.0.0.1:8001/find_cafe_review?cafeName={cafeName}"
            else:
                url = ""
                print("No cafe name found in quotes.")
            
        else:
            self.intent_type = "default_embedding_search"
            url = f"http://127.0.0.1:8001/search?keyword={self.search_input}"
        
        return url
            
                    
    # def simulate_loading(self):
    #     print("Loading...")
    #     time.sleep(1)
    #     print("Loading complete!")
        