from openai import OpenAI
from fastapi import FastAPI, Query
from neo4j import GraphDatabase
from typing import List, Dict
import json
import time

app = FastAPI()

# connectionString = "neo4j://172.23.151.30:7687" [personal]
# username = "neo4j" # [personal]
# password = "neo4j123" [personal]

# connectionString = "neo4j://172.20.254.108:7687" # [office]
# username = "neo4j"
# password = "sandbox123"

connectionString = "neo4j://172.26.177.45:7687" # [VM]
username = "neo4j"
password = "neo4j123"


driver = GraphDatabase.driver(connectionString, auth=(username, password))

client = OpenAI(api_key="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", )

async def get_search_results(keyword: str) -> List[Dict[str, str]]:
    results = []
    
    embedding = client.embeddings.create(input=[keyword], model = 'text-embedding-ada-002').data[0].embedding
    
    # 27/12/2023 - 5 TOP MATCH REVIEWS
    # Returns top 5 matching reviews with their cafe details, sorted by highest search score
    query = (
        "CALL db.index.vector.queryNodes('review-embeddings', 20, $embedding) YIELD node AS review, score "
        "WITH review, score "
        "MATCH (cafe:Cafe)-[:HAS_REVIEW]->(review) "
        "WITH cafe, review.review AS reviewContent, score AS maxScore "
        "RETURN "
            "cafe.name AS cafeName, "
            "cafe.rating AS cafeRating, "
            "COALESCE(cafe.city, 'N/A') AS city, "
            "COALESCE(cafe.rate_for_two, 'N/A') AS rateForTwo, "
            "reviewContent AS reviews "
        "ORDER BY maxScore DESC "
        "LIMIT 10; "
    )
    
    start_time = time.time()
    with driver.session() as session:
        for record in session.run(query, embedding=embedding):
            result = dict(record)
            results.append(result)
        # print(results)  
    end_time = time.time()
    print(f"\n NEO4J = Time taken: {end_time - start_time} seconds \n\n")
    
    formatted_json = json.dumps(results, indent=2)
    
    print("==============SEARCH RESULT JSON======================")
    print(formatted_json)
    print("==============SEARCH RESULT JSON======================")
    
    driver.close()
    return results


@app.get("/search")
async def search(keyword: str):
    search_results = await get_search_results(keyword)
    
    
    # Prompt for more than 1 cafe review returned
    user_prompt = (
        f"Here are the reviews of the cafes that match user's search query '{keyword}':\n\n'{search_results}\n\n"
        "Recommend the most suitable cafe based on the user query based on the reviews"
        "and details, considering ambiance, and highlighted aspects in the reviews."
        "Provide a 60 word recommendation for users to make an informed decision."
    )
    
    start_time = time.time()
    
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        temperature=0.7,
        # max_tokens=150,
        messages=[
            {"role": "system", "content": "You are a Cafe recommendation expert."},
            {"role": "user", "content": user_prompt}
        ]
    )    
    end_time = time.time()
    print(f"\n OpenAI = Time taken: {end_time - start_time} seconds \n\n")
    
    formatted_results = [
        {
            "cafeName": row["cafeName"],
            "city": row["city"],
            "cafeRating": row["cafeRating"],
            "rateForTwo": row["rateForTwo"],
            "reviews": row["reviews"],
        }
        for row in search_results
    ]
    
    response_data = {
        "data": formatted_results,
        "openai_summary": response.choices[0].message.content
    }
    
    return response_data



# ============================================================================================
#    No need featured snippet
# ============================================================================================
def find_cafe_by_cuisine(cuisine_type: str) -> List[Dict[str, str]]:
    results = []
    
    query = (
        "MATCH (cafe:Cafe)-[:SERVES]->(cuisine:Cuisine) "
        "WHERE toLower(cuisine.type) =~ toLower($cuisine_type) AND cafe.rating IS NOT NULL "
        "WITH "
            "cafe, "
            "cuisine, "
            "COLLECT(DISTINCT cuisine.type) AS matchedCuisines "
        "RETURN "
            "cafe.name AS cafeName, "
            "cafe.rating AS cafeRating, "
            "COALESCE(cafe.city, 'N/A') AS city, "
            "COALESCE(cafe.rate_for_two, 'N/A') AS rateForTwo, "
            "matchedCuisines AS cuisines "
        "ORDER BY cafe.rating DESC "
        "LIMIT 10 "

    )
    
    with driver.session() as session:
        for record in session.run(query, cuisine_type=cuisine_type):
            result = dict(record)
            results.append(result)
            
    driver.close()
    return results

@app.get("/find_cafe_by_cuisine")
def find_cafe_by_cuisine_endpoint(cuisine_type:str):
    search_results = find_cafe_by_cuisine(cuisine_type)
    return search_results

# ============================================================================================
#     No need featured snippet
# ============================================================================================
def find_top_rated_cafe() -> List[Dict[str, str]]:
    results = []
    
    query = (
        "MATCH (cafe:Cafe)-[:SERVES]->(cuisine:Cuisine) "
        "WHERE cafe.rating IS NOT NULL "
        "RETURN "
            "cafe.name AS cafeName, "
            "cafe.rating AS cafeRating, "
            "COALESCE(cafe.city, 'N/A') AS city, "
            "COALESCE(cafe.rate_for_two, 'N/A') AS rateForTwo, "
            "COLLECT(DISTINCT cuisine.type) AS cuisines "
        "ORDER BY cafe.rating DESC "
        "LIMIT 10 "
    )
    
    with driver.session() as session:
        for record in session.run(query):
            result = dict(record)
            results.append(result)
        
    driver.close() 
    return results

@app.get("/find_top_rated_cafe")
def find_top_rated_cafe_endpoint():
    search_results = find_top_rated_cafe()
    return search_results

# ============================================================================================
#        needs search results
# ============================================================================================
async def find_top_rated_cafe_by_cuisine(cuisine_type: str) -> List[Dict[str, str]]:
    top_cafe_results = []
    
    query2 = (
        "MATCH (cafe:Cafe)-[:SERVES]->(cuisine:Cuisine {type: $cuisine_type}) "
        "WHERE toLower(cuisine.type) =~ toLower($cuisine_type) AND cafe.rating IS NOT NULL "
        "WITH cafe "
        "ORDER BY cafe.rating DESC "
        "LIMIT 1 "
        "MATCH (cafe)-[:HAS_REVIEW]->(review:Review) "
        "RETURN cafe.name AS cafeName, cafe.rating AS cafeRating, cafe.city As city, coalesce(cafe.rate_for_two, 'N/A') as rateForTwo, collect(review.review) AS reviews"
    )
    
    with driver.session() as session:
        for record in session.run(query2, cuisine_type=cuisine_type):
            result = dict(record)
            top_cafe_results.append(result)
            # print(top_cafe_results)
            
    formatted_json = json.dumps(top_cafe_results, indent=2)
    
    return top_cafe_results


@app.get("/find_top_rated_cafe_by_cuisine")
async def find_top_rated_cafe_by_cuisine_endpoint(input: str, cuisine_type: str):
    top_cafe_results = await find_top_rated_cafe_by_cuisine(cuisine_type)
    
    formatted_json = json.dumps(top_cafe_results, indent=2)
    
    # CURRENTLY USING
    # user_prompt = f"Here are the reviews for the top-rated cafe based on user's search query '{input}':\n\n'{top_cafe_results}\n\nNow, summarize the reviews, including both positive and negative points. Provide an overall impression, mentioning the cafe name, rating, and rate for two(if available). Leverage your cafe review expertise to craft a well-phrased 60 words summary."
    # 
    
    user_prompt = (
         f"Here are the reviews for the top-rated cafe based on user's search query '{input}':\n\n'{top_cafe_results}\n\n"
         "Now, summarize the reviews, including both positive and negative points." 
         "Provide an overall impression, mentioning the cafe name, rating, and rate for two(if available)."
         "Leverage your cafe review expertise to craft a well-phrased 60 words summary."
        #  "Specify if there are no reviews for this cafe."
    )
    
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        temperature=0.7,
        # max_tokens=150,
        messages=[
            {"role": "system", "content": "You are a Cafe review expert."},
            {"role": "user", "content": user_prompt}
        ]
    )
    
    formatted_results = [
        {
            "cafeName": row["cafeName"],
            "cafeRating": row["cafeRating"],
            "city": row["city"],
            "rateForTwo": row["rateForTwo"],
            "reviews": row["reviews"],
            # "summary": response['choices'][0]['message']['content'],
            "summary": response.choices[0].message.content
        }
        for row in top_cafe_results
    ]
    
    print(json.dumps(formatted_results, indent=2))
        
    return formatted_results    





# ============================================================================================

async def find_cafe_reviews(cafeName: str) -> List[Dict[str, str]]:
    results = []
    
    query = (
        "MATCH (cafe:Cafe {name: $cafeName})-[:SERVES]->(cuisine:Cuisine) "
        "OPTIONAL MATCH (cafe)-[:HAS_REVIEW]->(review:Review) "
        "RETURN "
            "cafe.name AS cafeName, "
            "cafe.rating AS cafeRating, "
            "COALESCE(cafe.city, 'N/A') AS city, "
            "COALESCE(cafe.rate_for_two, 'N/A') AS rateForTwo, "
            "COLLECT(DISTINCT cuisine.type) AS cuisines, "
            "COLLECT(DISTINCT review.review) AS reviews "
    )
    
    with driver.session() as session:
        for record in session.run(query, cafeName=cafeName):
            result = dict(record)
            results.append(result)

    driver.close() 
    
    print(f"cafeName: {cafeName}")
    
    print("00000000000000000000000000000000000000000")
    print(json.dumps(results,indent=2))
    print("00000000000000000000000000000000000000000")
    
    return results






@app.get("/find_cafe_review")
async def find_cafe_review_endpoint(cafeName: str):
    cafe_review_results = await find_cafe_reviews(cafeName)
    
    # user_prompt = f"Here is the cafe name '{cafeName}':\n\n'{cafe_review_results}\n\nNow, summarize the reviews, including both positive and negative points. Provide an overall impression, mentioning the cafe name, rating, and rate for two(if available). Leverage your cafe review expertise to craft a well-phrased 60 words summary."
    user_prompt = f"Here is the cafe name '{cafeName}':\n\n'{cafe_review_results}\n\nNow, summarize the reviews, including both positive and negative points. Provide an overall impression, mentioning the cafe name, rating, and rate for two(if available). Leverage your cafe review expertise to craft a well-phrased 60 words summary."

    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        temperature=0.7,
        messages=[
            {"role": "system", "content": "You are a Cafe review expert."},
            {"role": "user", "content": user_prompt}
        ]
    )
    
    formatted_results = {
        "data": cafe_review_results,
        "openai_summary": response.choices[0].message.content
    }
    
    
    return formatted_results    