"""Welcome to Reflex! This file outlines the steps to create a basic app."""
from rxconfig import config

from rag_system.components import navbar, sidebar, login, searchResult, featuredSnippet, knowledgepanel
from rag_system.state import State

import reflex as rx

def index() -> rx.Component:
    return rx.vstack(
        navbar(),
        sidebar(),
        rx.cond(
            State.alert_message != "",
            rx.alert(
                rx.alert_icon(),
                rx.alert_title(
                    State.alert_message
                ),
                status = "error",
            ),
        ),
        rx.cond(
            State.valid_input,
            rx.hstack(
                rx.box(width="23%"),
                rx.vstack(
                    featuredSnippet(),
                    searchResult(),
                    # reviews(),
                ),
                rx.cond(
                    State.has_kp,
                    knowledgepanel(),
                    None,
                ),
                rx.box(width="10%"), #acts a spacer
                justify="end", #moves the components to the right side(end)
                align_items="start", #align to top vertically
                justify_contents="center",
            ),
            rx.box(
                rx.spinner(
                    color="lightgreen",
                    thickness=5,
                    speed="0.5s",
                    size="xl",
                ),
            ),
        ),
        justify="start",
    )
    
# ORI PAGE ORDER TEMPLATE
# def index() -> rx.Component:
#     return rx.vstack(
#         navbar(),
#         sidebar(),
#         rx.cond(
#             State.alert_message != "",
#             rx.alert(
#                 rx.alert_icon(),
#                 rx.alert_title(
#                     State.alert_message
#                 ),
#                 status = "error",
#             ),
#         ),
#         rx.cond(
#             State.valid_input,
#             rx.hstack(
#                 rx.box(width="23%"),
#                 rx.vstack(
#                     featuredSnippet(),
#                     searchResult(),
#                     # reviews(),
#                 ),
#             ),
#             rx.box(
#                 rx.spinner(
#                     color="lightgreen",
#                     thickness=5,
#                     speed="0.5s",
#                     size="xl",
#                 )
#             )
#         )
#     )
    
def loginpage() -> rx.Component:
    return rx.vstack(
        login(),
    )
    

# Add state and page to the app.
app = rx.App()
app.add_page(index)
app.add_page(loginpage, route="/login")
# app.compile()
