# CafeRecommendationSystem
This repository contains the source code for the Cafe Recommendation System project. Follow the steps below to set up the project locally and run the API and Reflex development server.

**Getting Started**
1. Clone Repository | Clone the repository to your local machine using the following command: `git clone https://gitlab.com/plurplemonster/rag-project.git`


2. Navigate to Project Directory | Navigate to the cloned directory: `cd rag-project`


3. Initialize Reflex | Once in the directory, initialize Reflex using the following command: `reflex init`

4. Open Project in VS Code | Open VS Code with the following command: `code .`

5. Create venv and install requirements.txt

6. Activate Virtual Environment | Activate the virtual environment (venv) for the project: `source .venv/bin/activate`


**Starting the API**
To start the API, follow these steps:

1. Navigate to rag_system Folder | Navigate to the rag_system folder: `cd rag_system`


2. Start the API | Start the API server with the following command: `uvicorn api:app --reload --port 8001`


3. Verify API Functionality | Check if the API is working by accessing the following link in your browser: `http://127.0.0.1:8001/docs`


**Running Reflex**
To run Reflex Web App, follow these steps:

1. Run Reflex | Run the following command to start the Reflex development server: `reflex run`


3. View Reflex Webpage | View the Reflex webpage by accessing the following link in your browser: `http://localhost:3000`


